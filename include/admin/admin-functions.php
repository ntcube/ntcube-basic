<?php
/*
 *  Author: NTCUBE
 *  URL: ntcube.com | @ntcube
 *  - Admin Functions
 */



/*------------------------------------*\
	Admin page
\*------------------------------------*/


//admin login start

/**
 * style-login lnk
 */
function my_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_template_directory_uri() . '/include/admin/css/style-login.css' );
}



/**
 * style-login logo
 */
function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
        background-image: url('/img/logo.png');
		height:50px;
		width:auto;
		background-size: contain;
		background-repeat: no-repeat;
        padding-bottom: 30px;
        }
    </style>
<?php }

/**
 * login logo link
 */
function my_login_logo_url() {
    return home_url();
}

/**
*login logo title
 */
function my_login_logo_url_title() {
    return 'Go home';
}

/**
*login redirect url
 */
function my_login_redirect( $redirect_to, $request, $user ) {
	//is there a user to check?
	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		//check for admins
		if ( in_array( 'administrator', $user->roles ) ) {
			// redirect them to the default place
			return $redirect_to;
		} else {
			return home_url();
		}
	} else {
		return $redirect_to;
	}
}
//admin login end


//-manaul

add_action('admin_menu', 'ntcube_basic_manaul');
 
function ntcube_basic_manaul() {
    add_submenu_page(
        'index.php',
        'Ntcube Manaul Options',
        '설명서',
        'read',
        'ntcube-basic-manual',
        'ntcube_manaul_options' );
}
 
function ntcube_manaul_options() {
	if ( !current_user_can( 'read' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.', 'ntcube-basic') );
	}
	require_once('manual.php');
}


/** remove menu */
function remove_menus() {
//  remove_menu_page('index.php' );                  //Dashboard
  remove_menu_page('edit.php' );                   //Posts
  remove_menu_page('upload.php' );                 //Media
  remove_menu_page('edit-comments.php' );          //Comments
  remove_menu_page('tools.php' );                  //Tools
  remove_menu_page('profile.php' );                  //profile
}

add_action('admin_menu', 'remove_menus');



// update toolbar
function update_adminbar($wp_adminbar) {

  // remove unnecessary items
  $wp_adminbar->remove_node('site-name');
  $wp_adminbar->remove_node('wp-logo');
  $wp_adminbar->remove_node('customize');
  $wp_adminbar->remove_node('comments');
    $wp_adminbar->remove_node('new-content');

//** add nt_manual menu item **//
  $wp_adminbar->add_node([
    'id' => 'nt_homepage',
    'title' => '홈페이지',
    'href' => '/',
    'meta' => [
    ]
  ]);
//** add nt_manual menu item **//
  $wp_adminbar->add_node([
    'id' => 'nt_dashboard',
    'title' => '관리자페이지',
    'href' => '/wp-admin/admin.php?page=nt-dashboard',
    'meta' => [
    ]
]);  

}

// admin_bar_menu hook
add_action('admin_bar_menu', 'update_adminbar', 80);


// admin footer
add_filter('admin_footer_text', 'left_admin_footer_text_output'); //left side
function left_admin_footer_text_output($text) {
    $text = '제작: NTCUBE | 대표번호: (1670-2441) | 이메일: official@ntcube.co.kr<br>
    엔티큐브 공식 홈페이지: <a href="https://ntcube.co.kr" target="_blank">바로가기</a> | 작업요청게시판: <a href="https://ntcube.co.kr/sub5_c2" target="_blank">바로가기</a>';
    return $text;
}
?>