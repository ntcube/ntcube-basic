<div id="admin-body-box">
<div class="dash-col2">
<div class="ntcube-dashbaord-sec1">
<ul class="ntcube-dashbaord-sec1-ul">
     <li>관리자페이지</li>
    <ul>
        <li>관리자페이지: 관리자 메인 화면입니다.</li>
        <li>업데이트관리: 테마, 플러그인의 업데이트 관리를 하실 수 있습니다. <b>(주의: mangboard 플러그인 업데이트 금지)</b></li>
    </ul>
    <li>컨텐츠관리</li>
    <ul>
        <li>게시판관리: 게시판을 관리할 수 있습니다.</li>
        <li>회원관리: 회원관리페이지입니다. (회원가입 및 회원기능이 없을경우 관리자회원 관리로 사용될 수 있습니다.)</li>
        <li>파일관리: 게시판으로 등록된 파일들을 관리할 수 있습니다.</li>
    </ul>
    <li>방문자관리</li>
    <ul>
        <li>로그관리: 접속로그 및 오류로그를 확인할 수 있습니다.</li>
        <li>통계관리: 간단한 사이트 통계를 확인할 수 있습니다.</li>
        <li>접속경로: 홈페이지 유입 경로를 확인할 수 있습니다.</li>
        <li>접속ip관리: 사이트에 접속ip를 차단/허용 할수 있습니다.</li>
    </ul>
    <li>페이지</li>
    <ul>
        <li>페이지: 홈페이지내 페이지목록 입니다.</li>
    </ul>
    <li>외모</li>
    <ul>
        <li>테마: 테마를 설치,선택 등 관리 할 수 있습니다. 삭제는 불가합니다.(특별한 경우 아니면 수정 금지!)</li>
        <li>사용자 정의: 테마와 연결된 사용자 정의기능입니다. 수정은 가능하나 사이트 디자인에 영향이 있습니다.</li>
        <li>위젯: 사이트의 기능을 담당하는 위젯을 관리할 수 있습니다. </li>
    </ul>
    <li>플러그인</li>
    <ul>
        <li>플러그인을 활성/비활성을 관리할 수 있습니다. (주의: MangBoard Admin Menu 플러그인를 먼저 비활성 해주세요.) 삭제 불가.</li>
    </ul>
    <li>사용자</li>
    <ul>
        <li>기본 워드프레스 회원 목록입니다.</li>
        <li>프로필: 워드프레스 계정 프로필을 관리합니다. 관리자페이지 계정 비밀번호 변경을 할 수있습니다.</li>
    </ul>
</ul>
    </div>
</div>
<div class="dash-col2">
<div style="width: 500px;">
    <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/268786131?color=f0c800&title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script></div></div>
    
</div>