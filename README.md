# KO
# [Ntcube Basic] WordPress Theme

**제작: 엔티큐브 (NTCUBE)
**URI: http://ntcube.co.kr
**설치필수버전:** WordPress 4.4  
**최신테스트 버전:** WordPress 4.9.8 
	
    
== 설명 ==

초기 사이트제작을 위한 워드프레스 테마입니다.
Ntcube Basic은 functions.php에 시간 절약 기능과 제작에필요한 소스파일이 미리 탑재되어있어 디자이너와 개발자에게 빠른 사이트 제작을 지원합니다. HTML/CSS를 워드프레스의 깔끔한 기본 제작 테마로 변환, 기본 드롭다운 헤더메뉴에 워드프레스 메뉴가 연동되어있습니다. 최소화된 소스로 로딩속도를 개선했습니다. 페이지 로딩속도 1초미만입니다.
데모사이트: https://nt-theme.ntcube.co.kr


== 설치방법 ==

1. 워드프레스 관리자페이지 접속, 외모 > 테마 > 여기서 Add New(추가) 버튼 클릭.
2. 테마파일을 선택후 (zip파일)  Install Now 버튼으로 테마 설치.
3. 설치된 테마를 활성화 합니다.


== 특징 ==

* 반응형 세팅
* 최소화된 코드와 깔끔하게 정리된 php 주석처리
* 슬라이딩 모바일 사이드메뉴 탑재 (mobile-menu.js)
* 개발자를 위한 사용자 정의 스크립트 파일 탑재 (scripts.js)
* @media를 사용하여 즉시 개발할 수있는 미디어 쿼리 프레임 워크
* Stylesheet 파일 함수 연결
* 사전정의된 함수들 (functions.php)
* 페이지 개발을 위한 사용자정의 템플릿 제작가능
* 사용자 정의가능한 검색폼 (searchform.php) - 전부수정가능
* swiper 슬라이드 기본 탑재
* 지속적인 업데이트 관리
* 깔끔한 워드프레스 관리자 화면 UI 디자인
* 한글지원


== 저작권 (Copyright) ==

# [Ntcube Basic](https://ntcube.co.kr) License
라이센스: GNU GPL
URL : https://ntcube.co.kr
E-mail : official@ntcube.co.kr

Theme is built using the following resource bundles:

-Fonts:
    notosanskr - http://www.google.com/fonts/earlyaccess > https://fonts.google.com/specimen/Noto%20Sans%20KR	
	License URL: http://fonts.gstatic.com/ea/notosanskr/v2/OFL.txt
    License: SIL OPEN FONT LICENSE Version 1.1 - 26 February 2007

-Ntcube-Baisc Themes Self Designed Images:
      /ntcube-basic/images/icons/touch.png
      /ntcube-basic/images/gravatar.jpg
          
Declaring these self designed images under GPL license version 2.0
License URL: http://www.gnu.org/licenses/gpl-2.0.html













##### ------------------------------------------------------------



# EN
# [Ntcube Basic] WordPress Theme

**Author: NTCUBE
**Author URI: http://ntcube.co.kr
**Requires at least:** WordPress 4.4  
**Tested up to:** WordPress 4.9.8 
	
	
== Description ==

Blank wordpress theme
Pre-loaded with time saving features in functions.php ,Ntcube-basic lets you ship your themes quicker, for designer and publisher.
Convert HTML/CSS To WordPress ,Balnkpage and clean code ,Dropdown header menu
Demo: https://nt-theme.ntcube.co.kr


== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.


== Features ==

* Responsive Ready
* Clean, neatly organised code, with PHP annotations
* Sliding mobile menu (mobile-menu.js)
* DOM Ready JavaScript file setup (scripts.js) for instant JavaScript development
* Media Queries framework for instant development using @media
* Stylesheet enqueued using WordPress functions into wp_head
* Preloaded Functions (functions.php)
* Demo Custom Page Template for expansion
* Custom Search Form included (searchform.php) - fully editable


== Copyright ==

# [Ntcube Basic](https://ntcube.co.kr) License
License: GNU GPL
URL : https://ntcube.co.kr
E-mail : official@ntcube.co.kr

Theme is built using the following resource bundles:

-Fonts:
    notosanskr - http://www.google.com/fonts/earlyaccess > https://fonts.google.com/specimen/Noto%20Sans%20KR	
	License URL: http://fonts.gstatic.com/ea/notosanskr/v2/OFL.txt
    License: SIL OPEN FONT LICENSE Version 1.1 - 26 February 2007

-Ntcube-Baisc Themes Self Designed Images:
      /ntcube-basic/images/icons/touch.png
      /ntcube-basic/images/gravatar.jpg
          
Declaring these self designed images under GPL license version 2.0
License URL: http://www.gnu.org/licenses/gpl-2.0.html







