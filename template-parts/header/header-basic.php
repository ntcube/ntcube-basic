<!-- nav -->
<div class="header-box">
    <div class="logo">
        <a href="<?php echo home_url(); ?>">
            <img src="/img/logo.png" alt="Logo" class="logo-img">
        </a>
    </div>
    <!-- /logo -->
    <nav class="nav" role="navigation">
        <?php  
         wp_nav_menu(array(  
           'menu' => 'Main Navigation', 
           'container_id' => 'dropdownmenu', 
           'menu_class' => 'nav desktop-menu',
           'walker' => new Ntcube_Dropdown_Menu()
          )); 
        ?>
    </nav>
</div>
<!-- /nav -->

<!--Mobile menu -->
<div id="mo-menuwarp">
    <a class="ninja-btn2" title="menu"><span></span></a>
    <div class="mo-menu-logo">
        <a href="/">
            <img src="/img/logo.png">
        </a>
    </div>
    <div class="panel left">
        <div class="panel left">
            <div class="panel-head">
                <!-- toggle -->
                <a class="ninja-btn" title="menu"><span></span></a>
                <!-- In slide menu dec-->
                <div class="panel-menu-logo">
                    <a href="/">
                        <img src="/img/logo.png">
                    </a>
                </div>
            </div>
            <nav class="nav" role="navigation">
                <?php ntmobile_nav('mobile-menu'); ?>
            </nav>
        </div>

        <!--Mobile menu outside background-->
        <div class="panel-overlay"></div>
    </div>
</div>
<!--Mobile menu -->
