<!-- nav -->
<div class="header-box">
    <div class="logo">
        <a href="<?php echo home_url(); ?>">
            <img src="/img/logo.png" alt="Logo" class="logo-img">
        </a>
    </div>
    <ul class="gnbmenu">
        <li><a href="#">대분류</a></li>
        <li><a href="#">대분류</a></li>
        <li><a href="#">대분류</a></li>
        <li><a href="#">대분류</a></li>
        <li><a href="#">대분류</a></li>
        <li><a href="#">대분류</a></li>
        <div class="navlist">
            <div class="navlist_2">
                <div class="navlist_3">
                    <ul>
                        <li><a class="sm-link sm-link_padding-bottom sm-link3" href="/sub1_c1"><span class="sm-link__label">중분류</span></a></li>
                        <li><a class="sm-link sm-link_padding-bottom sm-link3" href="/sub1_c2"><span class="sm-link__label">중분류</span></a></li>
                        <li><a class="sm-link sm-link_padding-bottom sm-link3" href="sub1_c3"><span class="sm-link__label">중분류</span></a></li>
                    </ul>
                    <ul>
                        <li><a class="sm-link sm-link_padding-bottom sm-link3" href="/sub2_c1"><span class="sm-link__label">중분류</span></a></li>
                        <li><a class="sm-link sm-link_padding-bottom sm-link3" href="/sub2_c2"><span class="sm-link__label">중분류</span></a></li>
                        <li><a class="sm-link sm-link_padding-bottom sm-link3" href="/sub2_c3"><span class="sm-link__label">중분류</span></a></li>
                    </ul>
                    <ul>
                        <li><a class="sm-link sm-link_padding-bottom sm-link3" href="/sub6_c1"><span class="sm-link__label">중분류</span></a></li>
                    </ul>
                    <ul>
                        <li><a class="sm-link sm-link_padding-bottom sm-link3" href="/sub3_c1"><span class="sm-link__label">중분류</span></a></li>
                        <li><a class="sm-link sm-link_padding-bottom sm-link3" href="/sub3_c2"><span class="sm-link__label">중분류</span></a></li>
                    </ul>
                    <ul>
                        <li><a class="sm-link sm-link_padding-bottom sm-link3" href="/sub5_c4"><span class="sm-link__label">중분류</span></a></li>
                    </ul>
                    <ul>
                        <li><a class="sm-link sm-link_padding-bottom sm-link3" href="/sub5_c1"><span class="sm-link__label">중분류</span></a></li>
                        <li><a class="sm-link sm-link_padding-bottom sm-link3" href="/sub5_c2"><span class="sm-link__label">중분류</span></a></li>
                        <li><a class="sm-link sm-link_padding-bottom sm-link3" href="/sub5_c5"><span class="sm-link__label">중분류</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </ul>
</div>
<!-- /nav -->

<!--Mobile menu -->
<div id="mo-menuwarp">
    <a class="ninja-btn2" title="menu"><span></span></a>
    <div class="mo-menu-logo">
        <a href="/">
            <img src="/img/logo.png">
        </a>
    </div>
    <div class="panel left">
        <div class="panel left">
            <div class="panel-head">
                <!-- toggle -->
                <a class="ninja-btn" title="menu"><span></span></a>
                <!-- In slide menu dec-->
                <div class="panel-menu-logo">
                    <a href="/">
                        <img src="/img/logo.png">
                    </a>
                </div>
            </div>
            <nav class="nav" role="navigation">
                <?php ntmobile_nav('mobile-menu'); ?>
            </nav>
        </div>

        <!--Mobile menu outside background-->
        <div class="panel-overlay"></div>
    </div>
</div>
<!--Mobile menu -->

