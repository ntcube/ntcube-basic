<!-- copyright -->
<p class="copyright">
    &copy;
    <?php echo date('Y'); ?> Copyright
    <?php bloginfo('name'); ?>.
    <?php _e('Powered by', 'ntcube-basic'); ?>
    <a href="//wordpress.org" title="WordPress">WordPress</a> &amp; <a href="//ntcube.co.kr" title="NTCUBE">NTCUBE</a>.
</p>
<!-- /copyright -->
