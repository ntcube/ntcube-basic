<?php get_header(); ?>

	<main class="body-box">
		<!-- section -->
		<section>
			<h1><?php echo sprintf( __( '%s Search Results for ', 'ntcube-basic' ), $wp_query->found_posts ); echo get_search_query(); ?></h1>

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
