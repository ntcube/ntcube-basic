<?php 
/*
Template Name: Right Sidebar
*/
get_header(); ?>

<main id=main role="main" class="body-box">

    <div id="sidebar-templete">
        <!-- section -->
        <section>


            <?php if (have_posts()): while (have_posts()) : the_post(); ?>

            <!-- article -->
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                <?php the_content(); ?>

                <?php comments_template( '', true ); // Remove if you don't want comments ?>

                <br class="clear">

                <?php edit_post_link(); ?>

            </article>
            <!-- /article -->

            <?php endwhile; ?>

            <?php else: ?>

            <!-- article -->
            <article>

                <h2>
                    <?php _e( 'Sorry, nothing to display.', 'ntcube-basic' ); ?>
                </h2>

            </article>
            <!-- /article -->

            <?php endif; ?>

        </section>
        <!-- /section -->
    </div>
        <div id="left-sidebar">
        <?php get_sidebar(); ?>
    </div>
</main>


<?php get_footer(); ?>
