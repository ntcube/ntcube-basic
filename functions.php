<?php
/*
 *  Author: NTCUBE
 *  URL: ntcube.com | @ntcube
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	Admin Functions
\*------------------------------------*/
require_once('include/admin/admin-functions.php');

/*------------------------------------*\
	Actions + Filters
\*------------------------------------*/

// Add Actions
add_action('init', 'ntcube_header_scripts'); // Add Custom Scripts to wp_head
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'ntcube_styles'); // Add Theme Stylesheet
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'ntcubewp_pagination'); // Add Pagination
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );
add_action( 'login_enqueue_scripts', 'my_login_logo' );
add_action('wp_enqueue_scripts', 'add_css_scripts_styles' ); //Add dropdown menu
add_action( 'init', 'nt_register_mobile_menu' ); //Add mobile menu
add_action( 'wp_enqueue_scripts', 'ntcube_add_js_scripts' ); //mobile menu toggle
add_action( 'after_setup_theme', 'wpse_theme_setup' ); //title tag


// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'ntcubegravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter( 'login_headerurl', 'my_login_logo_url' );
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter( 'login_headertitle', 'my_login_logo_url_title' );
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images
add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether








/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/



/*------------------------------------*\
	Theme Support
\*------------------------------------*/

load_theme_textdomain( 'ntcube-basic', get_template_directory() . '/languages' );

if (!isset($content_width))
{
    $content_width = 900;
}

if (function_exists('add_theme_support'))
{
    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain( 'ntcube-basic' );
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// customlogo
$args = array(
  'width' => 290,
  'height' => 81,
  'header-text' => 'test-cls'
);
add_theme_support( 'custom-logo', $args );

//Custom-header
$defaults = array(
	'default-image'          => '',
	'width'                  => 0,
	'height'                 => 0,
	'flex-height'            => false,
	'flex-width'             => false,
	'uploads'                => true,
	'random-default'         => false,
	'header-text'            => true,
	'default-text-color'     => '',
	'wp-head-callback'       => '',
	'admin-head-callback'    => '',
	'admin-preview-callback' => '',
);
add_theme_support( 'custom-header', $defaults );


//Custom-background
$defaults = array(
	'default-color'          => '',
	'default-image'          => '',
	'default-repeat'         => 'repeat',
	'default-position-x'     => 'left',
    'default-position-y'     => 'top',
    'default-size'           => 'auto',
	'default-attachment'     => 'scroll',
	'wp-head-callback'       => '_custom_background_cb',
	'admin-head-callback'    => '',
	'admin-preview-callback' => ''
);
add_theme_support( 'custom-background', $defaults );
$args = array(
	'default-color' => '000000',
	'default-image' => '%1$s/images/background.jpg',
);
add_theme_support( 'custom-background', $args );



//Custom stylesheet add
function add_css_scripts_styles() {  
    wp_enqueue_style( 'swiper-styles', get_template_directory_uri() . '/css/swiper.min.css');
}

// Custom JS add
function ntcube_add_js_scripts() {
    wp_enqueue_script( 'ntcube-mobile-toggle', get_template_directory_uri() . '/js/mobile-menu.js', array('jquery') );
    wp_enqueue_script( 'full-wide-menu', get_template_directory_uri() . '/js/full-wide-menu.js', array('jquery') );
    wp_enqueue_script( 'ntcube-swiper', get_template_directory_uri() . '/js/swiper.min.js', array('jquery') );
}


// register a mobile menu
function nt_register_mobile_menu() {
    add_theme_support( 'nav-menus' );
    register_nav_menus( array('mobile-menu' => __( 'Mobile Menu', 'ntcube-basic' )) );
}

// Load Ntcube-Basic scripts (header.php)
function ntcube_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

        wp_register_script('ntcubescripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0'); // Custom scripts
        wp_enqueue_script('ntcubescripts'); // Enqueue it!
    }
}

// Load Ntcube-Basic styles
function ntcube_styles()
{
    wp_register_style('ntcube-basic', get_stylesheet_directory_uri() . '/style.css', array());
    wp_enqueue_style('ntcube-basic'); // Enqueue it!
}

function ntmobile_nav()
{
	wp_nav_menu(
	array(
        'theme_location'  => 'mobile-menu',
        'menu' => 'Mobile Menu', 
        'container_id' => 'dropdownmenu', 
        'menu_class' => 'nav mobile-menu',
        'walker' => new Ntcube_Dropdown_Menu()
		)
	);
}

// Ntcube-Basic navigation


function dropdownmenumaker_scripts_styles() {  
   wp_enqueue_style( 'dropdownmenu-styles', get_stylesheet_directory_uri() . '/css/nav-style.css');
}
class Ntcube_Dropdown_Menu extends Walker {

  var $db_fields = array( 'parent' => 'menu_item_parent', 'id' => 'db_id' );

  function start_lvl( &$output, $depth = 0, $args = array() ) {
    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<ul>\n";
  }

  function end_lvl( &$output, $depth = 0, $args = array() ) {
    $indent = str_repeat("\t", $depth);
    $output .= "$indent</ul>\n";
  }

  function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

    global $wp_query;
    $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
    $class_names = $value = '';        
    $classes = empty( $item->classes ) ? array() : (array) $item->classes;

    /* Add active class */
    if(in_array('current-menu-item', $classes)) {
      $classes[] = 'active';
      unset($classes['current-menu-item']);
    }

    /* Check for children */
    $children = get_posts(array('post_type' => 'nav_menu_item', 'nopaging' => true, 'numberposts' => 1, 'meta_key' => '_menu_item_menu_item_parent', 'meta_value' => $item->ID));
    if (!empty($children)) {
      $classes[] = 'has-sub';
    }

    $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
    $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

    $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
    $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

    $output .= $indent . '<li' . $id . $value . $class_names .'>';

    $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
    $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
    $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
    $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

    $item_output = $args->before;
    $item_output .= '<a'. $attributes .'><span>';
    $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
    $item_output .= '</span></a>';
    $item_output .= $args->after;

    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  }

  function end_el( &$output, $item, $depth = 0, $args = array() ) {
    $output .= "</li>\n";
  }
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Widget Area 1', 'ntcube-basic'),
        'description' => __('Description for this widget-area...', 'ntcube-basic'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('Widget Area 2', 'ntcube-basic'),
        'description' => __('Description for this widget-area...', 'ntcube-basic'),
        'id' => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}


// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function ntcubewp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Custom Excerpts
function ntcubewp_index($length) // Create 20 Word Callback for Index page Excerpts, call using ntcubewp_excerpt('ntcubewp_index');
{
    return 20;
}


// Create the Custom Excerpts callback
function ntcube_wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom View Article link to Post
function ntcube_view_article($more)
{
    global $post;
    return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'ntcube-basic') . '</a>';
}



// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function ntcubegravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/images/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function ntcubecomments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
<!-- heads up: starting < for the html tag (li or div) in the next line: -->
<<?php echo $tag ?>
    <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-
    <?php comment_ID() ?>">
    <?php if ( 'div' != $args['style'] ) : ?>
    <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
        <?php endif; ?>
        <div class="comment-author vcard">
            <?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
            <?php printf(('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
        </div>
        <?php if ($comment->comment_approved == '0') : ?>
        <em class="comment-awaiting-moderation">
            <?php ('Your comment is awaiting moderation.') ?></em>
        <br />
        <?php endif; ?>

        <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
                <?php
			printf( ('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a>
            <?php edit_comment_link(('(Edit)'),'  ','' );
		?>
        </div>

        <?php comment_text() ?>

        <div class="reply">
            <?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
        </div>
        <?php if ( 'div' != $args['style'] ) : ?>
    </div>
    <?php endif; ?>
    <?php }


/*------------------------------------*\
	Woocommerce
\*------------------------------------*/

//Woocommerce stylesheet
function ntcube_basic_theme_woocommerce_scripts() {
  wp_enqueue_style( 'custom-woocommerce-style', get_template_directory_uri() . '/woocommerce/css/custom-woocommerce.css' );
}
add_action( 'wp_enqueue_scripts', 'ntcube_basic_theme_woocommerce_scripts' );

////차일드테마 우커머스 스타일시트 선언 (차일드테마 functins.php 에 추가)
//function theme_woocommerce_scripts() {
//  wp_enqueue_style( 'custom-woocommerce-style', get_template_directory_uri() . '/woocommerce/css/custom-woocommerce.css' );
//    wp_enqueue_style( 'custom-woocommerce-child-style', get_stylesheet_directory_uri() . '/woocommerce/css/custom-woocommerce.css', array( 'custom-woocommerce-style' ) );
//}
//add_action( 'wp_enqueue_scripts', 'theme_woocommerce_scripts' );

//액션 선언
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

add_action('woocommerce_before_main_content', 'ntcube_basic_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'ntcube_basic_theme_wrapper_end', 10);

function ntcube_basic_theme_wrapper_start() {
  echo '<main id="main">';
}

function ntcube_basic_theme_wrapper_end() {
  echo '</main>';
}

//Woocommerce support
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce', array(
        'thumbnail_image_width' => 150,
        'single_image_width'    => 300,
        'product_grid'          => array(
        'default_rows'    => 3,
        'min_rows'        => 2,
        'max_rows'        => 8,
        'default_columns' => 4,
        'min_columns'     => 2,
        'max_columns'     => 5,
        ),
    ) );
}


/*------------------------------------*\
	
   User Passwd Check
\*------------------------------------*/

add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );


add_filter("retrieve_password_message", "ntcube_custom_password_reset", 99, 4);

function ntcube_custom_password_reset($message, $key, $user_login, $user_data )    {

$message = "누군가가 다음 계정의 패스워드 초기화를 요청했습니다: 

사용자명 : " . sprintf(__('%s'), $user_data->user_name) . "

이메일 : " . sprintf(__('%s'), $user_data->user_email) . "

만약 실수라면 이 이메일을 무시하셔도 패스워드가 변경되지 않습니다.

암호를 변경하려면 다음 주소를 방문하세요:

" . network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user_login), 'login') . "\r\n";


  return $message;

}
/*------------------------------------*\
    DB 오브젝트 변환 함수
\*------------------------------------*/
if(!function_exists('objectToArray')){
function objectToArray($d) {
        if (is_object($d)) {
            // Gets the properties of the given object
            // with get_object_vars function
            $d = get_object_vars($d);
        }

        if (is_array($d)) {
            /*
        * Return array converted to object
        * Using __FUNCTION__ (Magic constant)
        * for recursive call
        */
            return array_map(__FUNCTION__, $d);
        } else {
            // Return array
            return $d;
        }
    }
}


/*------------------------------------*\
	Theme Check
\*------------------------------------*/

function wpse_theme_setup() {
    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );
}

	function check( $php_files, $css_files, $other_files ) {
		$ret = true;
		// combine all the php files into one string to make it easier to search
		$php = implode( ' ', $php_files );
		checkcount();
		if (strpos( $php, 'paginate_comments_links' ) === false &&
			strpos( $php, 'the_comments_navigation' ) === false &&
			strpos( $php, 'the_comments_pagination' ) === false &&
		    (strpos( $php, 'next_comments_link' ) === false && strpos( $php, 'previous_comments_link' ) === false ) ) {
			$this->error[] = '<span class="tc-lead tc-required">'.__('REQUIRED','ntcube-basic').'</span>: '.__('The theme doesn\'t have comment pagination code in it. Use <strong>paginate_comments_links()</strong> or <strong>the_comments_navigation</strong> or <strong>the_comments_pagination</strong> or <strong>next_comments_link()</strong> and <strong>previous_comments_link()</strong> to add comment pagination.', 'ntcube-basic' );
			$ret = false;
		}
	}




/*------------------------------------*\
	Theme update  http://wp-updates.com
\*------------------------------------*/

require_once('wp-updates-theme.php');
new WPUpdatesThemeUpdater_2380( 'http://wp-updates.com/api/2/theme', basename( get_template_directory() ) );


?>
