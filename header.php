<!doctype html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
		<?php wp_head(); ?>

	</head>
	<body <?php body_class(); ?>>

<!-- wrapper -->
<div class="wrapper">
    <!-- header -->
    <header class="header clear" role="banner">
        <?php get_template_part( 'template-parts/header/header', 'basic' ); ?>
    </header>
    <!-- /header -->


