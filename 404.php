<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section>

			<!-- article -->
			<article id="post-404">

				<h1><i class="fa fa-exclamation-triangle" style="color: orangered;"></i> <?php _e( 'Page not found', 'ntcube-basic' ); ?></h1>
				<h2 style="padding: 0px;margin:0px;">
					<a href="<?php echo home_url(); ?>"><?php _e( 'Return home?', 'ntcube-basic' ); ?></a>
                    <a class="button-basic" href="<?php echo home_url(); ?>">Go Home</a>
				</h2>

			</article>
			<!-- /article -->

		</section>
		<!-- /section -->
	</main>


<?php get_footer(); ?>
